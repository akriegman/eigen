// This file is part of Eigen, a lightweight C++ template library
// for linear algebra.
//
// Copyright (C) 2014 Benoit Steiner <benoit.steiner.goog@gmail.com>
//
// This Source Code Form is subject to the terms of the Mozilla
// Public License v. 2.0. If a copy of the MPL was not distributed
// with this file, You can obtain one at http://mozilla.org/MPL/2.0/.

#include "main.h"

#include <Eigen/CXX11/Tensor>

using Eigen::Tensor;
using Eigen::DefaultDevice;

template <int DataLayout>
static void test_dynamic()
{
  Tensor<float, 3, DataLayout> epsilon(3, 3, 3);
  epsilon << 0, 0, 0,
             0, 0, 1,
             0,-1, 0,
           
             0, 0,-1,
             0, 0, 0,
             1, 0, 0,
           
             0, 1, 0,
            -1, 0, 0,
             0, 0, 0;
  Tensor<float, 2, DataLayout> a(3, 3);
  a <<       1, 1, 1,
             1, 2, 3,
             1, 4, 9;

  VERIFY_IS_APPROX(epsilon.contract(a.chip(0, 1), Pairs<1>{Pair{0, 0}})
                          .contract(a.chip(1, 1), Pairs<1>{Pair{0, 0}})
                          .contract(a.chip(2, 1), Pairs<1>{Pair{0, 0}}), 2);
  
  Tensor<float, 2, DataLayout> b(2, 2);
  VERIFY_RAISES_ASSERT(b << 1, 2, 3, 4, 5);
  b << 1, 2, 3, 4;
  VERIFY_IS_APPROX(b(0, 0), 1);
  VERIFY_IS_APPROX(b(0, 1), 2);
  VERIFY_IS_APPROX(b(1, 0), 3);
  VERIFY_IS_APPROX(b(1, 1), 4);
}

void test_cxx11_tensor_comma_initializer()
{
  CALL_SUBTEST(test_dynamic<ColMajor>());
  CALL_SUBTEST(test_dynamic<RowMajor>());
}
