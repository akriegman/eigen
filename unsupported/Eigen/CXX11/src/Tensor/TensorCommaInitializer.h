// Written by Aaron Kriegman <aaronkplus2@gmail.com>

#ifndef EIGEN_CXX11_TENSOR_TENSOR_COMMA_INITIALIZER_H
#define EIGEN_CXX11_TENSOR_TENSOR_COMMA_INITIALIZER_H

namespace Eigen {

/** \internal
  *
  * \class TensorCommaInitializer
  * \ingroup CXX11_Tensor_Module
  *
  * \brief Convenience class for tensor initialization.
  *
  * Some syntactic sugar for initializing tensors. See CommaInitializer in 
  * the core Eigen module.
  *
  * \sa CommaInitializer
  */

template<typename XprType>
struct TensorCommaInitializer {

  typedef typename XprType::Scalar Scalar;
  typedef typename XprType::Dimensions Dimensions;

  enum {
    Rank = XprType::NumDimensions
  };

  TensorCommaInitializer(XprType& xpr, const Scalar& s)
    : m_xpr(xpr), m_idx(), m_dims(xpr.dimensions()) {
    m_xpr(m_idx) = s;
  }

  EIGEN_DEVICE_FUNC EIGEN_STRONG_INLINE TensorCommaInitializer& operator,(const Scalar& s) {
    // This could be marginally improved with a recursive template?
    // I think this rollover incrementing approach is faster than
    // incrementing a single index and doing the math each time to
    // get the list of indices, because most times we only do the loop once.
    int dim = Rank - 1;
    while (true) {
      if (++m_idx[dim] == m_dims[dim]) {
        m_idx[dim--] = 0;
        eigen_assert(dim >= 0 && "Too many values passed to TensorCommaInitializer");
      }
      else break;
    }
    m_xpr(m_idx) = s;
    return *this;
  }

protected:
  XprType& m_xpr;
  Dimensions m_idx;
  Dimensions m_dims;
};

} // end namspace Eigen

#endif // EIGEN_CXX11_TENSOR_TENSOR_COMMA_INITIALIZER_H